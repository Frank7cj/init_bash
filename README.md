# init_bash

## Content

### 1) [choco_install.sh][2]

File for install apps with chocolatey after a Windows fresh installation, with optional devs apps.

#### Installation

Using an administrative shell:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

### 2) .bashrc for MinGW64

File for MinGW, with fixes execution of python and init work directory.

### 3) [.vimrc with PlugInstall][1]

File with PlugInstall extensions for bash.

#### Installation Unix

```sh
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

You can automate the process by putting the command in your Vim configuration
file as suggested [here][auto].

[auto]: https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation

#### Installation Windows (PowerShell)

```powershell
iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |`
    ni $HOME/vimfiles/autoload/plug.vim -Force
```

[1]: https://github.com/junegunn/vim-plug (PlugInstall oficial git)
[2]: https://chocolatey.org/install (Installing Chocolatey oficial web)
