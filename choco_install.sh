#!/bin/bash

# Install gsudo first
choco install gsudo vscode spotify googlechrome git 7zip discord steam sumatrapdf

# Devs Options
if ["$1" -eq "with-devs"]
then
    choco install python nodejs
fi
